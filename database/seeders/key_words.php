<?php

namespace Database\Seeders;

use App\Models\KeyWord;
use App\Models\Language;
use Illuminate\Database\Seeder;

class key_words extends Seeder
{
    public $keys = [
        'contact_us' => [
            'en' => 'CONTACT US',
            'ar' => 'تواصل معنا',
        ],
        'screen_shots' => [
            'en' => 'SCREEN SHOTS',
            'ar' => 'شلشات التطبيق',
        ],
        'advantages' => [
            'en' => 'ADVANTAGES',
            'ar' => 'مميزاتنا',
        ],
        'how_it_work' => [
            'en' => 'HOW IT WORK',
            'ar' => 'كيف يعمل',
        ],
        'home' => [
            'en' => 'HOME',
            'ar' => 'الرئيسية',
        ],
        'download_on_the' => [
            'en' => 'Download On The',
            'ar' => 'التحميل من',
        ],
        'lang' => [
            'en' => 'العربية',
            'ar' => 'English',
        ]
    ];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->keys as $key => $val)
        {
            $key_word = KeyWord::create(['key' => $key]);

            foreach ($val as $lang_key => $lang_val)
            {
                $language = Language::where('local_name' , $lang_key)->firstOrFail();
                $key_word->languages()->attach($language->id,['word' => $lang_val]);
            }
        }
    }
}
