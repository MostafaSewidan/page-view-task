<?php

namespace Database\Seeders;

use App\Models\Page;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(Pages::class);
        $this->call(settings::class);
        $this->call(languages::class);
        $this->call(key_words::class);
    }
}
