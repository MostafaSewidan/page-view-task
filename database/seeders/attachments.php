<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class attachments extends Seeder
{
    /**
     * @param $model
     * @param $data
     */
    public function run($model , $data)
    {
        $model->attachmentRelation()->create($data);
    }
}
