<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class settings extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::insert("INSERT INTO `settings_categories` (`id`, `name`, `level`, `created_at`, `updated_at`) VALUES
(1, 'links', 1, NULL, NULL),
(2, 'phones', 2, NULL, NULL),
(3, 'emails', 3, NULL, NULL),
(4, 'logos', 4, NULL, NULL)");

        DB::insert("INSERT INTO `settings` (`id`, `settings_category_id`, `key`, `value`, `display_name`, `data_type`, `level`, `created_at`, `updated_at`) VALUES
(1, 1, 'face', 'https://www.facebook.com/', 'facebook_link', 'text', 1, NULL, NULL),
(2, 1, 'twitter', 'https://twitter.com/', 'twitter_link', 'text', 1, NULL, NULL),
(3, 1, 'android_app', '#', 'android app', 'text', 1, NULL, NULL),
(4, 1, 'ios_app', '#', 'ios app', 'text', 1, NULL, NULL),
(5, 2, 'phone_1', '0123456789', 'phone 1', 'text', 1, NULL, NULL),
(6, 2, 'phone_2', ' 0123456789', 'phone 2', 'text', 1, NULL, NULL),
(7, 3, 'email', 'info@domain.com', 'email', 'email', 1, NULL, NULL),
(8, 4, 'logo', '/uploads/logo-new.png', 'logo', 'fileWithPreview', 1, NULL, NULL),
(9, 1, 'instagram', 'https://www.instagram.com/', 'instagram_link', 'text', 1, NULL, NULL),
(10, 1, 'moltaqa', 'https://moltaqa.net', 'Alkayan elaseel - moltaqa tech', 'text', 1, NULL, NULL)");
    }
}
