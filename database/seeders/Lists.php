<?php

namespace Database\Seeders;

use App\Models\SectionList;
use Illuminate\Database\Seeder;

class Lists extends Seeder
{

    public $lists = [
        'section-1-2' => [
            [
                'title_ar' => 'تسجيل فيديو',
                'title_en' => 'Record a video',
                'description_en' => 'Record an introductory video about your most important information and express your capabilities',
                'description_ar' => 'سجل فيديو تعريفي عن أهم معلوماتك ومعبر عن امكانياتك',
                'icon' => '<span class="icon-tools-2"></span>',
                'count' => 1,
                'level' => 1,
                'photo_data' => null
            ],
            [
                'title_ar' => 'تقدم للوظائف',
                'title_en' => 'Apply for jobs',
                'description_en' => 'Apply to suitable job offerings according to your pre-entered skills',
                'description_ar' => 'تقدم للوظائف المعروضة المناسبة وفقا لمهاراتك المدخلة مسبقا',
                'icon' => '<span class="icon-tools"></span>',
                'count' => 2,
                'level' => 2,
                'photo_data' => null
            ],
            [
                'title_ar' => 'القبول بالوظيفة',
                'title_en' => 'Job acceptance',
                'description_en' => 'Wait for companies to communicate with you via the chat app',
                'description_ar' => 'انتظر تواصل الشركات معك عبر الدردشة الخاصة بالتطبيق',
                'icon' => '<span class="icon-layers"></span>',
                'count' => 3,
                'level' => 3,
                'photo_data' => null
            ],
        ],

        'section-1-3' => [
            [
                'title_ar' => 'سهولة الاستخدام',
                'title_en' => 'Ease of use',
                'description_en' => '',
                'description_ar' => '',
                'icon' => '',
                'count' => null,
                'level' => null,
                'photo_data' => null
            ],
            [
                'title_ar' => 'الحفاظ علي خصوصة البيانات للمتقدمين',
                'title_en' => 'Maintaining the privacy of applicants\' data',
                'description_en' => '',
                'description_ar' => '',
                'icon' => '',
                'count' => null,
                'level' => null,
                'photo_data' => null
            ],
            [
                'title_ar' => 'سرعة الحصول علي الوظيفة',
                'title_en' => 'Quick access to the job',
                'description_en' => '',
                'description_ar' => '',
                'icon' => '',
                'count' => null,
                'level' => null,
                'photo_data' => null
            ],
            [
                'title_ar' => 'اتاحة الوصول لعدد كبير من الشركات',
                'title_en' => 'Providing access to a large number of companies',
                'description_en' => '',
                'description_ar' => '',
                'icon' => '',
                'count' => null,
                'level' => null,
                'photo_data' => null
            ],
        ],

        'section-1-7' => [
            [
                'title_ar' => '',
                'title_en' => '',
                'description_en' => '',
                'description_ar' => '',
                'icon' => '',
                'count' => null,
                'level' => null,
                'photo_data' => [
                    'name' => 'slider-1.png',
                    'path' => 'uploads/front/lists/slider-1.png',
                    'type' => 'image',
                    'mime_type' => 'png',
                ],
            ],
            [
                'title_ar' => '',
                'title_en' => '',
                'description_en' => '',
                'description_ar' => '',
                'icon' => '',
                'count' => null,
                'level' => null,
                'photo_data' => [
                    'name' => 'slider-2.png',
                    'path' => 'uploads/front/lists/slider-2.png',
                    'type' => 'image',
                    'mime_type' => 'png',
                ],
            ],
            [
                'title_ar' => '',
                'title_en' => '',
                'description_en' => '',
                'description_ar' => '',
                'icon' => '',
                'count' => null,
                'level' => null,
                'photo_data' => [
                    'name' => 'slider-3.png',
                    'path' => 'uploads/front/lists/slider-3.png',
                    'type' => 'image',
                    'mime_type' => 'png',
                ],
            ],
            [
                'title_ar' => '',
                'title_en' => '',
                'description_en' => '',
                'description_ar' => '',
                'icon' => '',
                'count' => null,
                'level' => null,
                'photo_data' => [
                    'name' => 'slider-4.png',
                    'path' => 'uploads/front/lists/slider-4.png',
                    'type' => 'image',
                    'mime_type' => 'png',
                ],
            ],
            [
                'title_ar' => '',
                'title_en' => '',
                'description_en' => '',
                'description_ar' => '',
                'icon' => '',
                'count' => null,
                'level' => null,
                'photo_data' => [
                    'name' => 'slider-5.png',
                    'path' => 'uploads/front/lists/slider-5.png',
                    'type' => 'image',
                    'mime_type' => 'png',
                ],
            ],
            [
                'title_ar' => '',
                'title_en' => '',
                'description_en' => '',
                'description_ar' => '',
                'icon' => '',
                'count' => null,
                'level' => null,
                'photo_data' => [
                    'name' => 'slider-6.png',
                    'path' => 'uploads/front/lists/slider-6.png',
                    'type' => 'image',
                    'mime_type' => 'png',
                ],
            ],
            [
                'title_ar' => '',
                'title_en' => '',
                'description_en' => '',
                'description_ar' => '',
                'icon' => '',
                'count' => null,
                'level' => null,
                'photo_data' => [
                    'name' => 'slider-7.png',
                    'path' => 'uploads/front/lists/slider-7.png',
                    'type' => 'image',
                    'mime_type' => 'png',
                ],
            ],
            [
                'title_ar' => '',
                'title_en' => '',
                'description_en' => '',
                'description_ar' => '',
                'icon' => '',
                'count' => null,
                'level' => null,
                'photo_data' => [
                    'name' => 'slider-8.png',
                    'path' => 'uploads/front/lists/slider-8.png',
                    'type' => 'image',
                    'mime_type' => 'png',
                ],
            ],
            [
                'title_ar' => '',
                'title_en' => '',
                'description_en' => '',
                'description_ar' => '',
                'icon' => '',
                'count' => null,
                'level' => null,
                'photo_data' => [
                    'name' => 'slider-9.png',
                    'path' => 'uploads/front/lists/slider-9.png',
                    'type' => 'image',
                    'mime_type' => 'png',
                ],
            ],
            [
                'title_ar' => '',
                'title_en' => '',
                'description_en' => '',
                'description_ar' => '',
                'icon' => '',
                'count' => null,
                'level' => null,
                'photo_data' => [
                    'name' => 'slider-10.png',
                    'path' => 'uploads/front/lists/slider-10.png',
                    'type' => 'image',
                    'mime_type' => 'png',
                ],
            ],
        ],
    ];

    /**
     * @param $section
     */
    public function run($section)
    {
        $lists = $this->lists[$section->name];

        if (count($lists)) {

            foreach ($lists as $list) {

                $model = $section->lists()->create([
                    'title_en' => $list['title_en'],
                    'description_en' => $list['description_en'],
                    'title_ar' => $list['title_ar'],
                    'description_ar' => $list['description_ar'],
                    'icon' => $list['icon'],
                    'count' => $list['count'],
                    'level' => $list['level'],
                ]);

                if ($list['photo_data']) {
                    (new attachments())->run($model , $list['photo_data']);
                }
            }
        }
    }
}
