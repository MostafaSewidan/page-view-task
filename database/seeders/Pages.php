<?php

namespace Database\Seeders;

use App\Models\Page;
use App\Models\Section;
use Illuminate\Database\Seeder;

class Pages extends Seeder
{
    public $pages = [
        [
            'title_en' => 'home',
            'title_ar' => 'الرئيسية',
            'name' => 'home',
            'view_domain' => 'home.index',
        ],
    ];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->pages as $page)
        {
            $model = Page::create($page);

            (new Sections())->run($model);
        }
    }
}
