<?php

namespace Database\Seeders;

use App\Models\Language;
use Illuminate\Database\Seeder;

class languages extends Seeder
{
    public $langs = [
        [
            'title' => 'english',
            'local_name' => 'en',
        ],
        [
            'title' => 'arabic',
            'local_name' => 'ar',
        ],
    ];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->langs as $lang)
        {
            Language::create($lang);
        }
    }
}
