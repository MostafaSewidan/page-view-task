<?php

namespace Database\Seeders;

use App\Models\Section;
use Illuminate\Database\Seeder;

class Sections extends Seeder
{
    public $sections = [
        'home' => [
            [
                'name' => 'section-1-1',
                'title_ar' => 'من نحن',
                'title_en' => 'WHO ARE WE',
                'description_ar' => 'تطبيق " vcv" يقدم الباحثين عن العمل بأسلوب جديد عن طريق تسجيل حساب خاص بالمتقدم يتضمن فيديو تعريفي للباحث بالإضافة الي البيانات الشخصية والاجتماعية و المؤهلات والخبرات السابقة ،بما يتيح إضافة معلومات قيمة تسهل على الجهة الباحثة عن فرص توظيف تنقية الاختيارات وتوفير الوقت، وأيضاً تساعد على تحديد مدى توافق المتقدمين مع الوظيفة قبل عمل مقابلة شخصية "interview"، وهي طريقة لم يسبق استخدامها ولم تطرح من قبل في سوق العمل، ولذلك يعد تطبيق "vcv" ألية توظيف جديدة لسلسة من القطاعات الحكومية والخاصة وللباحثين عن العمل على مستوى العالم تبحث عن وظيفة؟ أنشأ حساب خاص بك وقم بإضافة فيديو تعريفي واختار الباقة المميزة',
                'description_en' => 'The "vcv" application provides job seekers in a new way by registering an account for the applicant that includes an introductory video for the researcher , in addition to personal and social data, qualifications and previous experiences, allowing the addition of valuable information to facilitate the searcher It is about employment opportunities, purifying options and saving time, and also helping to determine the extent of applicants ’compatibility with the job before conducting an“ interview ”, a method that has not been used previously and has not been introduced before in the labor market. Therefore, the application of“ vcv ”is a new recruitment mechanism for a series of sectors. Governmental, private and for job seekers globally looking for a job?',
                'level' => 1,
                'has_lists' => false,
                'photo_data' => [
                    'name' => 'section-1.png',
                    'path' => 'uploads/front/sections/section-1.png',
                    'type' => 'image',
                    'mime_type' => 'png',
                ],
            ],
            [
                'name' => 'section-1-2',
                'title_ar' => 'كيف يعمل',
                'title_en' => 'HOW IT WORKS',
                'description_ar' => 'التطبيق سهل الإستخدام فهي خطوات بسيطة للتعامل مع التطبيق',
                'description_en' => 'The application is easy to use, as they are simple steps to deal with the application',
                'level' => 2,
                'has_lists' => true,
                'photo_data' => null,
            ],
            [
                'name' => 'section-1-3',
                'title_ar' => 'مميزاتنا',
                'title_en' => 'OUR ADVANTAGES',
                'description_ar' => 'يشمل التطبيق على العديد من المميزات الجيدة',
                'description_en' => 'The application includes many good features',
                'level' => 3,
                'has_lists' => true,
                'photo_data' => [
                    'name' => 'section-3.png',
                    'path' => 'uploads/front/sections/section-3.png',
                    'type' => 'image',
                    'mime_type' => 'png',
                ],
            ],
            [
                'name' => 'section-1-7',
                'title_ar' => 'شاشات التطبيق',
                'title_en' => 'SCREENSHOTS',
                'description_ar' => 'بعض شاشات التطبيق تبين سهولة التعامل و جمال التنسيق',
                'description_en' => 'Some application screens show the ease of handling and the beauty of coordination',
                'level' => 4,
                'has_lists' => true,
                'photo_data' => null,
            ],
            [
                'name' => 'section-1-12',
                'title_ar' => 'تواصل معنا',
                'title_en' => 'Contact Us',
                'description_ar' => 'تواصل معنا بالطرق المختلفة او بالاتصال او بالبريد الإلكتروني او السوشيال ميديا',
                'description_en' => 'Contact us by various means, or by calling, e-mail, or social media',
                'level' => 5,
                'has_lists' => false,
                'photo_data' => null,
            ],
        ]
    ];

    /**
     * @param $page
     */
    public function run($page)
    {
        $sections = $this->sections[$page->name];

        if (count($sections)) {
            foreach ($sections as $section) {
                $model = $page->sections()->create([
                    'name' => $section['name'],
                    'title_ar' => $section['title_ar'],
                    'title_en' => $section['title_en'],
                    'description_en' => $section['description_en'],
                    'description_ar' => $section['description_ar'],
                    'level' => $section['level'],
                ]);

                if ($section['has_lists']) {
                    (new Lists())->run($model);
                }
//
                if ($section['photo_data']) {
                    (new attachments())->run($model , $section['photo_data']);
                }
            }
        }
    }
}
