<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePagesTable extends Migration {

	public function up()
	{
		Schema::create('pages', function(Blueprint $table) {
			$table->increments('id');
			$table->string('title_en');
			$table->string('title_ar');
			$table->string('name');
			$table->string('view_domain');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('pages');
	}
}