<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSectionsTable extends Migration {

	public function up()
	{
		Schema::create('sections', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('page_id');
			$table->string('name');
			$table->string('title_en');
			$table->string('title_ar');
			$table->text('description_en')->nullable();
			$table->text('description_ar')->nullable();
			$table->tinyInteger('is_active')->default('1');
			$table->integer('level')->nullable();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('sections');
	}
}