<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateListsTable extends Migration {

	public function up()
	{
		Schema::create('lists', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('section_id');
			$table->string('title_en')->nullable();
			$table->string('title_ar')->nullable();
            $table->string('description_en')->nullable();
            $table->string('description_ar')->nullable();
            $table->string('icon')->nullable();
            $table->integer('count')->nullable();
            $table->tinyInteger('is_active')->default('1');
            $table->integer('level')->nullable();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('lists');
	}
}