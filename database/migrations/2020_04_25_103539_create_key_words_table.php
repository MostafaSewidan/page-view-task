<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateKeyWordsTable extends Migration {

	public function up()
	{
		Schema::create('key_words', function(Blueprint $table) {
			$table->increments('id');
			$table->string('key');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('key_words');
	}
}