<!DOCTYPE html>
<!-- saved from url=(0019)http://vcvjobs.com/ -->
<html lang="en-US">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <title>تطبيق فيديو سي في - </title>
    <meta name="description" content="Pixa - App Landing Page Pack with Page builder">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="shortcut icon" type="image/x-icon" href="{{asset($settings->logo)}}">

    <!-- STYLESHEETS -->
    <link rel="stylesheet" href="{{asset('/front/bootstrap.min.css')}}" type="text/css" media="all">
    <link rel="stylesheet" href="{{asset('/front/font-awesome.min.css')}}" type="text/css" media="all">
    <link rel="stylesheet" href="{{asset('/front/et-line.css')}}" type="text/css" media="all">
    <link rel="stylesheet" href="{{asset('/front/magnific-popup.css')}}" type="text/css" media="all">
    <link rel="stylesheet" href="{{asset('/front/slick.css')}}" type="text/css" media="all">
    <link rel="stylesheet" href="{{asset('/front/hover-min.css')}}" type="text/css" media="all">
    <link rel="stylesheet" href="{{asset('/front/style.css')}}" type="text/css" media="all">

    @if(session()->get('lang') == 'ar')
        <link rel="stylesheet" href="{{asset('/front/style-rtl.css')}}" type="text/css" media="all">
    @endif
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

</head>
<body data-new-gr-c-s-check-loaded="14.1002.0" data-gr-ext-installed="">