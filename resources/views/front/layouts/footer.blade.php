
<footer id="footer-1" class="footer">
    <div class="container">

        <!-- footer menu -->
        <ul class="footer-menu">
            <li>
                <a href="{{url('/#section-1-1')}}">@lang('lang.home')</a>
            </li>
            <li>
                <a href="{{url('/#section-1-2')}}">@lang('lang.how_it_work')</a>
            </li>
            <li>
                <a href="{{url('/#section-1-3')}}">@lang('lang.advantages')</a>
            </li>
            <li>
                <a href="{{url('/#section-1-7')}}">@lang('lang.screen_shots')</a>
            </li>
            <li>
                <a href="{{url('/#section-1-12')}}">@lang('lang.contact_us')</a>
            </li>
            <li>
                <a href="{{url('/home/'.\Helper\Helper::reverseLocal())}}">@lang('lang.lang')</a>
            </li>
        </ul>
        <!-- copyright text -->
        <span class="copyright">All Rights Received 2020 <br>
        <a href="{{$settings->moltaqa}}">Alkayan elaseel - moltaqa tech</a></span>

        <!-- social icons -->
        <ul class="social-icons">
            <li><a href="{{$settings->face}}"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
            <li><a href="{{$settings->twitter}}"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
            <li><a href="{{$settings->instagram}}"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
        </ul>

    </div>
</footer>