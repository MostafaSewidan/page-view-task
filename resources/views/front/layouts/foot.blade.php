
<!-- SCRIPTS -->
<script type="text/javascript" src="{{asset('front/jquery-1.12.3.min.js')}}"></script>
<script type="text/javascript" src="{{asset('front/jquery.easing.min.js')}}"></script>
<script type="text/javascript" src="{{asset('front/jquery.single-page.min.js')}}"></script>
<script type="text/javascript" src="{{asset('front/jquery.magnific-popup.min.js')}}"></script>
<script type="text/javascript" src="{{asset('front/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('front/jquery.counterup.min.js')}}"></script>
<script type="text/javascript" src="{{asset('front/jquery.ajaxchimp.js')}}"></script>
<script type="text/javascript" src="{{asset('front/countdown.js')}}"></script>
<script type="text/javascript" src="{{asset('front/waypoints.min.js')}}"></script>
<script type="text/javascript" src="{{asset('front/scrollreveal.min.js')}}"></script>
<script type="text/javascript" src="{{asset('front/classie.js')}}"></script>
<script type="text/javascript" src="{{asset('front/custom.js')}}"></script>
<script type="text/javascript" src="{{asset('front/slick.min.js')}}"></script>

</body>
</html>