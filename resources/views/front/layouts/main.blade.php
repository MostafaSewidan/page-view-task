@include('front.layouts.head')
@include('front.layouts.header')

@yield('content')

@include('front.layouts.footer')
@include('front.layouts.foot')