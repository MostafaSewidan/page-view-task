
<div class="preloader-outer" style="display: none;">
    <div class="preloader" aria-busy="true" aria-label="Loading, please wait." role="progressbar"></div>
</div>
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button aria-controls="navbar" aria-expanded="false" class="navbar-toggle collapsed" data-target="#navbar" data-toggle="collapse" type="button"><span class="sr-only">Toggle
            navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span></button>

            <!-- Logo -->
            <a class="navbar-brand current" href="http://vcvjobs.com/#section-1-1">
                <img src="{{asset($settings->logo)}}" alt="homepage" class="light-logo">
            </a>

            <!-- Logo end -->

        </div>
        <div class="navbar-collapse collapse" id="navbar">

            <div class="navbar-right">

                <ul class="nav navbar-nav">
                    <li>
                        <a href="{{url('/#section-1-1')}}" class="current">@lang('lang.home')</a>
                    </li>
                    <li>
                        <a href="{{url('/#section-1-2')}}">@lang('lang.how_it_work') </a>
                    </li>
                    <li>
                        <a href="{{url('/#section-1-3')}}">@lang('lang.advantages')</a>
                    </li>
                    <li>
                        <a href="{{url('/#section-1-7')}}">@lang('lang.screen_shots')</a>
                    </li>
                    <li>
                        <a href="{{url('/#section-1-12')}}">@lang('lang.contact_us')</a>
                    </li>
                </ul>

                <!-- Menu end -->

                <!-- Social Icons -->
                <ul class="nav navbar-nav social">
                    <li><a href="{{$settings->face}}" class="external"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                    <li><a href="{{$settings->twitter}}" class="external"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>

                    <li><a href="{{url('/home/'.\Helper\Helper::reverseLocal())}}" class="external">{{ucfirst(\Helper\Helper::reverseLocal())}}</a></li>
                </ul>

            </div>

        </div><!--/.nav-collapse -->
    </div>
</nav>
