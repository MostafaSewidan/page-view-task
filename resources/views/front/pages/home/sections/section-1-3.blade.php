<section id="section-1-3" class="describe-1">
    <div class="container">
        <div class="row flex"><!-- Row begin -->
            <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="image">
                    <img src="{{$section->attachment}}" alt="describe" class="b20-1" data-sr-id="2"
                        >
                </div>
            </div>
            <div class="col-md-6 col-sm-12 col-xs-12">
                <h2 class="light">{{$section->trans->title}}</h2>
                <p>
                    {!! $section->trans->description !!}
                </p>

                @if(count($lists))
                    <ul class="list-style pb-15">

                        @foreach($lists as $list)
                            <li>{{$list->trans->title}}</li>
                        @endforeach

                    </ul>
                @endif
            </div>
        </div>
    </div>
</section>