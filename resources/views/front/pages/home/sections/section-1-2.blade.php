<section id="section-1-2" class="work-process colored">
    <div class="container">

        <div class="section-header text-center">
            <h2>{{$section->trans->title}}</h2>
            <p>{!! $section->trans->description !!}</p>
        </div>

        @if(count($lists))
            <div class="row">
                @foreach($lists as $list)
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="item text-center">
                            <div class="icon-wrap">
                                <span class="step s1-1" data-sr-id="5" >{{$list->count}}</span>
                                {!! $list->icon !!}
                            </div>
                            <h3>{{$list->trans->title}}</h3>
                            <p>{!! $list->trans->description !!}</p>
                        </div>
                    </div>
                @endforeach
            </div>
        @endif
    </div>
</section>