
<section id="section-1-12" class="download-cta colored">
    <div class="container container-800 z-1 relative">
        <div class="cta text-center">
            <h2 class="light">{{$section->trans->title}}</h2>
            <p>{!! $section->trans->description !!}</p>

            <ul class="contact-list">
                <li><a href="mailto:{{$settings->email}}"><i class="fa fa-envelope" aria-hidden="true"></i> {{$settings->email}}</a></li>
                <li><a href="tel:0123456789"><i class="fa fa-phone" aria-hidden="true"></i> {{$settings->phone_1}}</a></li>
                <li><a href="tel:0123456789"><i class="fa fa-phone" aria-hidden="true"></i> {{$settings->phone_2}}</a></li>
            </ul>

            <!-- buttons -->
            <div class="cta-buttons">
                <a href="{{$settings->ios_app}}" class="btn btn-primary btn-download hvr-float-shadow">
                    <i class="fa fa-apple" aria-hidden="true"></i>
                    <span class="text">
						<span class="little">@lang('lang.download_on_the')</span><br>App Store
					</span>
                </a>
                <a href="{{$settings->android_app}}" class="btn btn-primary btn-download hvr-float-shadow">
                    <i class="fa fa-android" aria-hidden="true"></i>
                    <span class="text">
						<span class="little">@lang('lang.download_on_the')</span><br>Google Play
					</span>
                </a>
            </div>

        </div>

    </div>
</section>