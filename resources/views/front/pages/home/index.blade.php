@extends('front.layouts.main')

@section('content')

    @foreach($sections as $section)
        @include('front.pages.home.sections.'.$section->name , [
        'section' => $section ,
         'lists' => $section->lists()->where('is_active' , 1)->orderBy('level')->get()
         ])
    @endforeach

@endsection