<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Auth::routes();

Route::get('/', 'HomeController@index');


Route::get('/clear-cache', function () {

    for ($i = 0; $i < 4; $i++) {
        Artisan::call('route:clear');
        Artisan::call('view:clear');
        Artisan::call('cache:clear');
        Artisan::call('config:clear');
        Artisan::call('config:cache');
    }

    return 'done you can go back and complete steps';
});

Route::get('/migrate-seed', function () {

    Artisan::call('migrate:seed');

    return 'done you can go back and complete steps';
});