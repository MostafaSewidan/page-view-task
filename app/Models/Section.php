<?php

namespace App\Models;

use App\Traits\GetAttribute;
use Illuminate\Database\Eloquent\Model;

class Section extends Model 
{
    use GetAttribute;

    protected $table = 'sections';
    public $timestamps = true;
    protected $fillable = array('page_id', 'title_en', 'description_ar','title_en', 'description_ar', 'level','is_active');

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->trans_cols = ['title','description'];
    }

    public function page()
    {
        return $this->belongsTo(Page::class);
    }

    public function lists()
    {
        return $this->hasMany(SectionList::class);
    }

}