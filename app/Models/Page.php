<?php

namespace App\Models;

use App\Traits\GetAttribute;
use Illuminate\Database\Eloquent\Model;

class Page extends Model 
{
    use GetAttribute;

    protected $table = 'pages';
    public $timestamps = true;
    protected $fillable = array('title_en','title_ar', 'name','view_domain');

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->trans_cols = ['title'];
    }

    public function sections()
    {
        return $this->hasMany(Section::class);
    }
}