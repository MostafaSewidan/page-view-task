<?php

namespace App\Models;

use App\Traits\GetAttribute;
use Illuminate\Database\Eloquent\Model;

class SectionList extends Model
{
    use GetAttribute;

    protected $table = 'lists';
    public $timestamps = true;
    protected $fillable = array('section_id','section_id','title_en', 'description_ar','title_en', 'description_ar','icon','count');

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->trans_cols = ['title','description'];
    }

    public function sections()
    {
        return $this->belongsTo(Section::class);
    }

}