<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Language extends Model
{

    protected $table = 'languages';
    public $timestamps = true;
    protected $fillable = array('title', 'local_name','is_active');

    public function keyWords()
    {
        return $this->belongsToMany(KeyWord::class)->withPivot('word');
    }
}