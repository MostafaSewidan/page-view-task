<?php

namespace App\Http\Middleware;

use App\Models\Page;
use Closure;
use Illuminate\Support\Facades\Auth;

class CheckSetDatabase
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try{
            $page = Page::where('name', 'home')->firstOrFail();

            return $next($request);
        }catch (\Exception $e)
        {
            return redirect('/');
        }
    }
}
