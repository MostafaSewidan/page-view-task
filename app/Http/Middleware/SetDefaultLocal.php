<?php

namespace App\Http\Middleware;

use App\Models\Page;
use Closure;
use Illuminate\Support\Facades\Auth;

class SetDefaultLocal
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!session()->get('lang'))
        {
            session()->put('lang' , 'ar');
            app()->setLocale('ar');
        }
        return $next($request);

    }
}
