<?php

namespace Front;

use App\Http\Controllers\Controller;
use App\Models\Language;
use App\Models\Page;
use App\Traits\Master;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class FrontController extends Controller
{
   use Master;

    public function __construct()
    {
        $this->model = new Page();
        $this->viewsDomain = 'front.pages.';
    }

    /**
     * @param $route_name
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index($route_name , Request $request){

        $page = $this->model->where('name', $route_name)->firstOrFail();
        $sections = $page->sections()->where('is_active' , 1)->orderBy('level')->get();
        return $this->view($page->view_domain , compact('page','sections'));
    }

    /**
     * @param $route_name
     * @param $lang
     * @return \Illuminate\Http\RedirectResponse
     */
    public function switchLang($route_name , $lang){

        $language = Language::where('local_name', $lang)->firstOrFail();
        session()->put('lang',$lang);

        return back();
    }
}
