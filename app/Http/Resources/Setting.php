<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use \App\Models\Setting as SettingModel;

class Setting extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */

    public function toArray($request)
    {
        $allSettings = \App\Models\Setting::all();
        $settingArray = [];
        foreach ($allSettings as $set) {
            $settingArray +=
                [
                    $set->key => $set->value
                ];
        }
        return $settingArray;
    }
}
